package com.epam.task03_oop.model;

public class Aircraft {
    private String type;
    private String reg;
    private String category;
    private int seat;
    private int cargo;
    private int range;
    private int fuelConsumption;

    public Aircraft(String line) {
        String[] parts = line.split(",");
        this.type = parts[0];
        this.reg = parts[1];
        this.category = parts[2];
        this.seat = Integer.parseInt(parts[3]);
        this.cargo = Integer.parseInt(parts[4]);
        this.range = Integer.parseInt(parts[5]);
        this.fuelConsumption = Integer.parseInt(parts[6]);
    }

    @Override
    public String toString() {
        return "Aircraft{" +
                "type='" + type + '\'' +
                ", reg='" + reg + '\'' +
                ", category='" + category + '\'' +
                ", seat=" + seat +
                ", cargo=" + cargo +
                ", range=" + range +
                ", fuelConsumption=" + fuelConsumption +
                '}';
    }

    public int getSeat() {
        return seat;
    }

    public int getCargo() {
        return cargo;
    }

    public String getType() {
        return type;
    }

    public String getReg() {
        return reg;
    }

    public String getCategory() {
        return category;
    }

    public int getRange() {
        return range;
    }

    public int getFuelConsumption() {
        return fuelConsumption;
    }
}
