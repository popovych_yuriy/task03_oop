package com.epam.task03_oop.model;

import java.util.List;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<Aircraft> getAircraftList() {
        return domain.getAircraftList();
    }

    @Override
    public int getAllSeats() {
        return domain.getAllSeats();
    }

    @Override
    public int getAllCargo() {
        return domain.getAllCargo();
    }

    @Override
    public void sortAircraftList() {
        domain.sortByRange();
    }

    @Override
    public List<Aircraft> findByFuelConsumption(int min, int max) {
        return domain.findByFuelConsumption(min, max);
    }
}
