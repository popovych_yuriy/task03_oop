package com.epam.task03_oop.model;

import java.util.List;

public interface Model {
    List<Aircraft> getAircraftList();

    int getAllSeats();

    int getAllCargo();

    void sortAircraftList();

    List<Aircraft> findByFuelConsumption(int min, int max);
}
