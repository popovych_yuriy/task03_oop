package com.epam.task03_oop.model;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Domain {
    private static final String FILENAME = "aircraft.csv";
    private File file;
    private List<Aircraft> aircraftList;

    public Domain() {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(FILENAME);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            file = new File(resource.getFile());
            try (FileReader reader = new FileReader(file);
                 BufferedReader br = new BufferedReader(reader)) {
                String line;
                aircraftList = new ArrayList<>();
                while ((line = br.readLine()) != null) {
                    aircraftList.add(new Aircraft(line));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Aircraft> getAircraftList() {
        return aircraftList;
    }

    public int getAllSeats() {
        int allSeats = 0;
        for (int i = 0; i < aircraftList.size(); i++) {
            allSeats += aircraftList.get(i).getSeat();
        }
        return allSeats;
    }

    public int getAllCargo() {
        int allCargo = 0;
        for (int i = 0; i < aircraftList.size(); i++) {
            allCargo += aircraftList.get(i).getCargo();
        }
        return allCargo;
    }

    public void sortByRange() {
        Collections.sort(aircraftList, new Comparator<Aircraft>() {
            @Override
            public int compare(Aircraft o1, Aircraft o2) {
                return o2.getRange() - o1.getRange();
            }
        });
    }

    public List<Aircraft> findByFuelConsumption(int min, int max) {
        List<Aircraft> result = new ArrayList<>();
        for (Aircraft aircraft : aircraftList) {
            if ((aircraft.getFuelConsumption() > min) && (aircraft.getFuelConsumption() < max)) {
                result.add(aircraft);
            }
        }
        return result;
    }
}
