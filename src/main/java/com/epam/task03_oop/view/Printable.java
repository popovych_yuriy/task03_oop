package com.epam.task03_oop.view;

@FunctionalInterface
public interface Printable {
    void print();
}
