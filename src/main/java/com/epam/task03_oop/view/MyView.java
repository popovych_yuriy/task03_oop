package com.epam.task03_oop.view;

import com.epam.task03_oop.controller.Controller;
import com.epam.task03_oop.controller.ControllerImpl;
import com.epam.task03_oop.model.Aircraft;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import static java.lang.Integer.*;

public class MyView {
    private static Scanner input = new Scanner(System.in);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", " 1 - Show the list of all planes");
        menu.put("2", " 2 - Show the number of seats in all planes");
        menu.put("3", " 3 - Show the sum of the cargo of all planes ");
        menu.put("4", " 4 - Sort by max range and show ");
        menu.put("5", " 5 - Find by fuel consumption ");
        menu.put("q", " Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton5() {
        System.out.print("Please input minimum fuel consumption value in l/km:\n");
        int min = parseInt(input.nextLine());
        System.out.print("Please input maximum fuel consumption value in l/km:\n");
        int max = parseInt(input.nextLine());
        List<Aircraft> aircraftList = controller.findByFuelConsumption(min, max);
        if ((aircraftList != null) && (!aircraftList.isEmpty())) {
            System.out.println("Planes in this range: ");
            for (Aircraft aircraft : aircraftList) {
                System.out.println(aircraft.toString());
            }
        } else {
            System.out.println("We don't have planes in this range of fuel consumption.");
        }
    }

    private void pressButton1() {
        System.out.println("All planes: ");
        for (Aircraft aircraft : controller.getAircraftList()) {
            System.out.println(aircraft.toString());
        }
    }

    private void pressButton2() {
        System.out.println("Seats in all planes: " + controller.getAllSeats());
    }

    private void pressButton3() {
        System.out.println("Cargo of all planes: " + controller.getAllCargo() + "t");
    }

    private void pressButton4() {
        System.out.println("Sorted by range list: ");
        for (Aircraft aircraft : controller.getSortedAircraftList()) {
            System.out.println(aircraft.toString());
        }
    }

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}
