package com.epam.task03_oop.controller;

import com.epam.task03_oop.model.Aircraft;
import com.epam.task03_oop.model.BusinessLogic;
import com.epam.task03_oop.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<Aircraft> getAircraftList() {
        return model.getAircraftList();
    }

    @Override
    public int getAllSeats() {
        return model.getAllSeats();
    }

    @Override
    public int getAllCargo() {
        return model.getAllCargo();
    }

    @Override
    public List<Aircraft> getSortedAircraftList() {
        model.sortAircraftList();
        return model.getAircraftList();
    }

    @Override
    public List<Aircraft> findByFuelConsumption(int min, int max) {
        return model.findByFuelConsumption(min, max);
    }
}
