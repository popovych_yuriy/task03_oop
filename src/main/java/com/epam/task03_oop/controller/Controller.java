package com.epam.task03_oop.controller;

import com.epam.task03_oop.model.Aircraft;

import java.util.List;

public interface Controller {
    List<Aircraft> getAircraftList();

    int getAllSeats();

    int getAllCargo();

    List<Aircraft> getSortedAircraftList();

    List<Aircraft> findByFuelConsumption(int min, int max);
}
